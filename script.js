const calcAverage = (a, b, c) => (a + b + c) / 3;

const avgdolphins = calcAverage(44, 23, 71);
const avgkoalas = calcAverage(65, 54, 49);

function checkWinner(avgdolphins, avgkoalas) {
  if (avgdolphins >= 2 * avgkoalas) {
    return `Dolphins win (${avgdolphins} vs ${avgkoalas})`;
  } else if (avgkoalas >= 2 * avgdolphins) {
    return `koalas win (${avgkoalas} vs ${avgdolphins})`;
  } else {
    return `no team wins`;
  }
}

const result = checkWinner(avgdolphins, avgkoalas);
console.log(result);
